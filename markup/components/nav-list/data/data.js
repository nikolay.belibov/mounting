var data = { navList: {
    items: [
        {
            name: 'РАБОТНИКИ',
            href: 'employees'
        },
        {
            name: 'ПРЕИМУЩЕСТВА',
            href: 'advantage'
        },
        {
            name: 'УСЛУГИ',
            href: 'service'
        },
        {
            name: 'СТОИМОСТЬ',
            href: 'cost'
        },
        {
            name: 'ВОПРОСЫ',
            href: 'questions'
        },
        {
            name: 'КОНТАКТЫ',
            href: 'contacts'
        },
    ]
}};
