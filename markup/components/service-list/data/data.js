var data = { serviceList: {
    items: [
        {
            icon: {
                name: 'ruler',
                class: 'service-list__icon'
            },
            text: 'Замер дверного проёма'
        },
        {
            icon: {
                name: 'drill',
                class: 'service-list__icon'
            },
            text: 'Монтаж дверей'
        },
        {
            icon: {
                name: 'saw',
                class: 'service-list__icon'
            },
            text: 'Демонтажные работы'
        },
        {
            icon: {
                name: 'engineer',
                class: 'service-list__icon'
            },
            text: 'Сервисное обслуживание'
        },
        {
            icon: {
                name: 'door',
                class: 'service-list__icon'
            },
            text: 'Межкомнатные двери'
        },
        {
            icon: {
                name: 'door-2',
                class: 'service-list__icon'
            },
            text: 'Входные двери'
        },
    ]
}};