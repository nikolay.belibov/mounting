let accordionSelector = document.querySelector('.accordion');

export default {
    init() {
        new Accordion(accordionSelector, {
            onToggle: (fold, isOpen) => {
                console.log(fold);
                console.log(isOpen);
            },
            noTransforms: true,
            disabled: false
        });
    },
};
