
import Swiper from 'swiper';

let swiperSelector = ('.swiper-container');

export default {
    init() {
        let carousel = new Swiper(swiperSelector, {
            // loop: true,
            slidesPerView: 1,
            spaceBetween: 0,
            pagination: {
                el: '.swiper-pagination',
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });
    },
};
