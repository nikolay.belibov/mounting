let map;

function init() {
    map = new ymaps.Map('map', {
        center: [55.76, 37.64],
        zoom: 9,
        controls: []
    });

    let coords = [
        [55.6763003, 37.5267814],
        [55.7900487, 37.5286922],
        [55.7090997, 37.6509069],
    ];
    let myCollection = new ymaps.GeoObjectCollection();

    for (let i = 0; i < coords.length; i++) {
        myCollection.add(new ymaps.Placemark(coords[i], {
            hintContent: ''
        }, {
            // Опции.
            iconLayout: 'default#image',
            iconImageHref: 'static/img/assets/map/map-marker.svg',
            iconImageSize: [30, 30],
            iconImageOffset: [-15, -15],
        }));
    }

    map.geoObjects.add(myCollection);

}
ymaps.ready(init);
