'use strict';
// Polyfills
import polyfills from './libraries/polyfills';
// Libs
import Accordion from 'accordion';
// Functions
import funcHeader from 'components/header/header';
import funcSwiper from 'components/swiper/swiper';
import funcAccordion from 'components/accordion/accordion';
import funcYmaps from 'components/map/map';
import funcRedstar from 'components/redstar/redstar';

$(() => {
    polyfills.init();
    // ================ Здесь инициализируем модули =====================
    funcHeader.init();
    funcSwiper.init();
    funcAccordion.init();
    funcRedstar.init();
});

funcHeader.scroll();
